# Ballet
This repository houses a compiler I'm working on for Ballet, a simple functional language.  Since I'm developing this as a way to teach myself about compiler and programming language design, I likely won't be accepting pull requests, but feel free to open issues if you have comments!

## Building the Ballet Compiler
The Ballet compiler is written in Rust and uses `cargo` for dependency management.  To build the compiler, run `cargo build`.  Compilation requires having the LLVM 10.0 development libraries available on your system.

## Running the Ballet Compiler
To run the Ballet Compiler, run `cargo run -- perform [BALLET SCRIPT]`.  In the future, further options will be made available.

## Language Features
Note: Many of these features are not yet implemented and/or are subject to change.

Ballet was designed to maximize developer ergonomics by not requiring the use of the Shift key.  It is a primarily functional language with an emphasis on prefix notation for function calling.

An basic example Ballet program is shown below:
```
def double a int end int
	.add a a

	def twice end int
		.double .double a
	end
end

.print .double 5           -- prints 10
.print .double.twice 5     -- prints 20

bind double7 /.double 7/  -- 'double7' is a function equivalent to 'double 7'

.print double7             -- prints 14
.print double7.twice       -- prints 28
```

### Functions
Functions are defined using the `def` keyword followed by the function's name and arguments (specified as a name, then a type).  The argument list ends with `end` and is followed by the return value of the function.  The end of the function definition is signified with another `end` keyword.

Within a function, there are both statements and member functions.  These are essentially independent of each other (see the section on bindings for more info).  When a function is executed on its own, it evaluates the statements of the function and returns the value of the final statement.  Member functions are both ways to namespace and to generate complex curried functions.  When a member function is called, it first binds the arguments of its parent before binding its own arguments.  This allows member functions to be called without needing to first bind the parent with proper values.

### Values
With a few exceptions (such as defining a function and binding a variable), everything in Ballet is either a function call, an argument to a function call, or both.  When a function which takes at least one argument is called, Ballet looks to the next value and does one of two things.  If that value is a "raw" value (i.e. it's not a function call), that value will be used as the argument.  If that value is a function, Ballet will consider the next values as arguments to this new function, and uses the returned data from this inner function call as an argument to the first function.  This process continues until every argument has been assigned a value.

### Conditionals
Conditional statements are also values in Ballet.  They are of the format `switch [condition] [true_val] [false_val]` where each argument after `switch` are individual Ballet values.  If `true_val` or `false_val` are multiple statements within the curry operator, they will be executed as if it was a single function with multiple statements called.  Both `true_val` and `false_val` must evaluate to the same type.

### Bindings
Unlike other languages, there is no = operator in Ballet.  Instead, to bind a value to a variable, the `bind` keyword is used.  It accepts two arguments: the name of the variable to attach to, and the value to assign to the variable.  If you're familiar with languages which used the 'let' keyword to assign values, you can think of `bind variablename value` as 'let variablename = value'.  Variables can be rebound as much as you want.

Due to the ability to bind values,

### Anonymous functions
A common occurrence in ballet is using an anonymous function, usually to create a curried function. To create an anonymous function, the operator `/` can be used.  Anything between two slashes are considered a new function, so all of `/something in here/` will be used as one value.  If you need to create an anonymous function inside of another anonymous function, multiple slashes can be used.  For example, `/outer function //an inner function// some more args/` passes `an inner function value` as an argument within the larger overall function.  There is no requirement that the number of slashes increase or decrease as adds more layers of nesting, but an inner nested function must end before its outer counterpart.

Anonymous functions cannot directly accept variables.  Instead, if a call's arguments aren't supplied within the anonymous function's definition, the anonymous function acts like a curried version of that function.  To accept a curried argument into a function, the type of an argument must also use slash notation.  For example, a function which takes a singe curried argument which must accept one int before returning a float can be defined with `def example curried /int float/ end`.  All values are actually functions which take no arguments, so you _could_ write a non-curried argument as `/type/`, though that's not required.

### Generics
To accept a generic type parameter, the `-` symbol is used.  When a type is prefixed with `-`, it's considered a generic type and the value following the `-` is bound to the type of the generic.  For example, `def generic a -gen end -gen` takes in a value of any type and returns something of that same type.

### Mutability
All values in Ballet are immutable.

### Purity
All functions are considered "pure" (i.e. they have no side effects) by default in Ballet.  If a function is impure, its return type must be surrounded with square brackets to indicate that it may cause side effects.  To minimize ambiguity to the user about the order in which impure functions execute, they cannot be used directly as arguments to a function and instead must first be bound to a variable.
#### Current state of impurity
Right now, the Ballet compiler has no concept of purity and likely will not for a while.  This means that all functions can be used as if they were pure with no checks.  It's likely a good idea to try to adhere to the purity guidelines if using Ballet for anything beyond experimentation, as otherwise it may be difficult to refactor code once pure functions are supported.

## Implemented features
Please note that this is by no means an exhaustive list.

### Implemented language features
- [X] Function definitions
- [X] Variable binding
- [X] Conditional switching
- [ ] Member functions
- [ ] Types
- [ ] Curried functions
- [ ] Generics
- [ ] Pure functions

### Implemented compiler features
- [X] Helpful error messages (sort of done)
- [ ] Optimizations as an AST
- [ ] Copy references to values
