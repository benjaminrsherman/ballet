use std::collections::HashMap;

use crate::frontend::*;
use crate::standard_library::compile_std;

use inkwell::builder::Builder;
use inkwell::context::Context;
use inkwell::module::Module;
use inkwell::passes::PassManager;
use inkwell::types::{BasicTypeEnum, FunctionType};
use inkwell::values::{BasicValue, BasicValueEnum, FunctionValue, GlobalValue, PointerValue};
use inkwell::AddressSpace;

/// General struct for holding LLVM state and references to current bindings.
/// There's currently a lot of error checking which occurs here that should
/// probably be moved to the Parser, but it's simpler to keep the error
/// detection here.
pub struct Compiler<'ctx> {
    pub context: &'ctx Context,
    pub builder: Builder<'ctx>,
    pub fpm: PassManager<FunctionValue<'ctx>>,
    pub module: Module<'ctx>,

    pub bindings: HashMap<String, BasicValueEnum<'ctx>>,
    pub string_names: HashMap<String, GlobalValue<'ctx>>,

    pub extern_fns: HashMap<String, FunctionValue<'ctx>>,

    print_llvm_bitcode: bool,
}

impl<'ctx> Compiler<'ctx> {
    /// Returns the function value for a given function name, if it exists.j
    fn get_function(
        &self,
        components: &AstFunctionPathComponents,
    ) -> Result<FunctionValue<'ctx>, String> {
        match self.module.get_function(&components.to_string()) {
            Some(f) => Ok(f),
            None => Err(format!("Unknown function '{}'", components.to_string())), // TODO: move to parser
        }
    }

    pub fn get_extern(&self, fn_name: &str) -> Option<&FunctionValue<'ctx>> {
        self.extern_fns.get(fn_name)
    }

    pub fn register_extern(
        &mut self,
        fn_name: &str,
        fn_type: FunctionType<'ctx>,
    ) -> FunctionValue<'ctx> {
        let func = self.module.add_function(fn_name, fn_type, None);
        self.extern_fns.insert(fn_name.to_string(), func);
        func
    }

    fn get_string_ptr(&mut self, string: &str) -> PointerValue<'ctx> {
        if let Some(ptr) = self.string_names.get(string) {
            return ptr.as_pointer_value();
        }

        let new_str_name = format!(".str.{}", self.string_names.len());
        let new_str = self.builder.build_global_string_ptr(string, &new_str_name);

        self.string_names.insert(string.to_string(), new_str);

        new_str.as_pointer_value()
    }

    pub fn new(context: &'ctx Context, print_llvm_bitcode: bool) -> Self {
        let module = context.create_module("ballet");
        let builder = context.create_builder();

        let fpm = PassManager::create(&module);
        // TODO: add fpm passes
        fpm.initialize();

        let mut compiler = Compiler {
            context,
            builder,
            module,
            fpm,
            bindings: HashMap::new(),
            string_names: HashMap::new(),
            extern_fns: HashMap::new(),
            print_llvm_bitcode,
        };

        compile_std(&mut compiler);

        compiler
    }

    /// Compiles a given set of functions and statements into a given LLVM module.
    /// Returns the LLVM value for the `main` function.
    pub fn compile(
        &mut self,
        functions: Vec<AstFunction>,
        statements: Vec<AstStatement>,
        generate_main_function: bool,
    ) {
        self.compile_signatures(&functions);
        self.compile_functions(&functions);

        if generate_main_function {
            self.generate_main_function(statements);
        }

        if self.print_llvm_bitcode {
            self.module.print_to_stderr();
        }
    }

    /// Registers the signatures for every provided function with the LLVM module.
    /// The function signatures must be registered before their corresponding functions
    /// are defined or called.
    fn compile_signatures(&self, functions: &Vec<AstFunction>) {
        for func in functions {
            self.compile_signature(&func.signature);

            // In the future, we'll want to deal with the namespacing issues of nested
            // definitions. Since we don't support namespaces yet, we just compile them
            // into the "global namespace" for now.
            if !func.functions.is_empty() {
                self.compile_signatures(&func.functions);
            }
        }
    }

    /// Registers a single signature into the function module.  Since we don't support
    /// types yet, this just assumes that every argument is a 64 bit signed integer.
    fn compile_signature(&self, signature: &AstSignature) {
        let i64_type = self.context.i64_type();
        // We don't support types yet (everything is a 64 bit integer)
        // so we just care about the number of arguments for now
        let arg_types = std::iter::repeat(i64_type)
            .take(signature.args.len())
            .map(|a| a.into())
            .collect::<Vec<BasicTypeEnum>>();
        let arg_types = arg_types.as_slice();

        let fn_type = match signature.ret_type {
            AstType::Nil => self.context.bool_type().fn_type(arg_types, false),
            AstType::Raw(AstRawType::float(_)) => self.context.f64_type().fn_type(arg_types, false),
            AstType::Raw(AstRawType::int(_)) => self.context.i64_type().fn_type(arg_types, false),
            AstType::Raw(AstRawType::str(_)) => self
                .context
                .i8_type()
                .ptr_type(AddressSpace::Generic)
                .fn_type(arg_types, false),
            AstType::Raw(AstRawType::bool(_)) => self.context.bool_type().fn_type(arg_types, false),
            AstType::Function(_) => panic!("Returning functions isn't supported yet"),
        };
        let fn_val =
            self.module
                .add_function(&signature.namespace_components.to_string(), fn_type, None);

        for (i, arg) in fn_val.get_param_iter().enumerate() {
            arg.into_int_value()
                .set_name(signature.args[i].name.as_str());
        }
    }

    /// Compiles all given functions into the module.  This does not re-register
    /// signatures, as it is assumed that the signatures for every function have already
    /// been registered.
    fn compile_functions(&mut self, functions: &Vec<AstFunction>) {
        for func in functions {
            self.compile_function(&func);
        }
    }

    /// Compiles a given function into the LLVM module.  Specifically, this will first
    /// compile each function within this function's namespace, then it will compile each
    /// call sequentially.
    /// TODO: There are many optimizations which can be performed here.  For example,
    /// the results of pure function calls can be stored or re-ordered to better optimize
    /// data flow and cache usage.  This restructuring may take place in the parser.
    fn compile_function(&mut self, func: &AstFunction) {
        // In the future, we'll want to deal with the namespacing issues of nested
        // definitions. Since we don't support namespaces yet, we just compile them
        // into the "global namespace" for now.
        self.compile_functions(&func.functions);

        if func.statements.is_empty() {
            return;
        }

        // Compile statements
        self.bindings.clear(); // We don't have any global variables (yet), so reset the bindings
        self.bindings.reserve(func.signature.args.len());

        let func_val = self
            .get_function(&func.signature.namespace_components)
            .unwrap();
        let entry = self.context.append_basic_block(func_val, "entry");
        self.builder.position_at_end(entry);

        // Set arguments
        for (i, arg) in func_val.get_param_iter().enumerate() {
            let arg_name = &func.signature.args[i].name;
            self.bindings.insert(arg_name.clone(), arg);
        }

        // We don't compile the last statement here as we build a return out
        // of the last statement in a function.
        for statement in &func.statements[0..func.statements.len() - 1] {
            match statement {
                AstStatement::Value(val) => {
                    self.compile_value(val);
                }

                AstStatement::Bind(ref name, ref val) => {
                    let bound_val = self.compile_value(val);
                    self.bindings.insert(name.clone(), bound_val);
                }
            }
        }

        match func.statements.last().unwrap() {
            AstStatement::Value(val) => {
                let ret_val = self.compile_value(val);
                self.builder.build_return(Some(&ret_val));
            }
            AstStatement::Bind(_, _) => {
                panic!(
                    "Error in function '{}': functions must end with a value",
                    func.signature.namespace_components.to_string()
                ); // TODO: move to parser
            }
        };

        if !func_val.verify(true) {
            self.module.print_to_stderr();
            panic!(
                "Invalid generated function for {}",
                func.signature.namespace_components.to_string()
            );
        }
    }

    fn generate_main_function(&mut self, statements: Vec<AstStatement>) {
        let main_func = AstFunction {
            signature: AstSignature {
                namespace_components: AstFunctionPathComponents(vec![
                    "".to_string(),
                    "__main".to_string(),
                ]),
                args: Vec::new(),
                ret_type: AstType::Nil,
            },
            functions: Vec::new(),
            statements,
        };

        self.compile_function(&main_func)
    }

    /// Compiles a function call.  Returns a BasicValueEnum representing the return value
    /// of the call.
    ///
    /// Ballet only currently supports functions with an i64 return value, so we can
    /// always expect a return value from this.
    fn compile_call(&mut self, call: &AstFunctionCall) -> BasicValueEnum<'ctx> {
        let target_func = self.get_function(&call.namespace_components).unwrap();

        let mut argsv = Vec::with_capacity(call.args.len());
        for arg in &call.args {
            argsv.push(self.compile_value(&arg));
        }

        self.builder
            .build_call(target_func, argsv.as_slice(), "tmp")
            .try_as_basic_value()
            .left()
            .unwrap()
    }

    fn compile_switch(
        &mut self,
        condition: &AstValue,
        true_branch: &AstValue,
        false_branch: &AstValue,
    ) -> BasicValueEnum<'ctx> {
        let curr_block = self.builder.get_insert_block().unwrap();

        // Generate blocks
        let llvm_true_branch = self
            .context
            .insert_basic_block_after(curr_block, "branch_true");
        let llvm_true_end = self
            .context
            .insert_basic_block_after(llvm_true_branch, "branch_true_end");
        let llvm_false_branch = self
            .context
            .insert_basic_block_after(llvm_true_end, "branch_false");
        let llvm_false_end = self
            .context
            .insert_basic_block_after(llvm_false_branch, "branch_false_end");
        let llvm_conditional_end = self
            .context
            .insert_basic_block_after(llvm_false_end, "branch_end");

        // Since conditions can only be booleans, we're free to cast this into an int value
        let llvm_cond_val = self.compile_value(condition).into_int_value();
        self.builder
            .build_conditional_branch(llvm_cond_val, llvm_true_branch, llvm_false_branch);

        self.builder.position_at_end(llvm_true_branch);
        let true_val = self.compile_value(true_branch);
        self.builder.build_unconditional_branch(llvm_true_end);
        self.builder.position_at_end(llvm_true_end);
        self.builder
            .build_unconditional_branch(llvm_conditional_end);

        self.builder.position_at_end(llvm_false_branch);
        let false_val = self.compile_value(false_branch);
        self.builder.build_unconditional_branch(llvm_false_end);
        self.builder.position_at_end(llvm_false_end);
        self.builder
            .build_unconditional_branch(llvm_conditional_end);

        self.builder.position_at_end(llvm_conditional_end);

        let phi = self.builder.build_phi(true_val.get_type(), "phi");
        phi.add_incoming(&[(&true_val, llvm_true_end), (&false_val, llvm_false_end)]);

        phi.as_basic_value()
    }

    /// Compiles a value type.  For a function call, this will generate an LLVM call
    /// instruction and return a BasicValueEnum holding the function's return value.
    /// For an identifier (binding), this will return a BasicValueEnum for that binding's
    /// value.  For a literal value, this will place that literal on the stack and
    /// return a BasicValueEnum holding that value.
    pub fn compile_value(&mut self, val: &AstValue) -> BasicValueEnum<'ctx> {
        match val {
            AstValue::Call { fn_call, .. } => self.compile_call(fn_call),
            AstValue::Ident { binding, .. } => *self.bindings.get(binding).unwrap(),
            AstValue::Switch {
                condition,
                true_branch,
                false_branch,
            } => self.compile_switch(condition, true_branch, false_branch),
            AstValue::Literal(literal) => match literal {
                AstRawType::int(val) => {
                    BasicValueEnum::IntValue(self.context.i64_type().const_int(*val as u64, true))
                }

                AstRawType::float(val) => {
                    BasicValueEnum::FloatValue(self.context.f64_type().const_float(*val as f64))
                }

                AstRawType::bool(true) => {
                    BasicValueEnum::IntValue(self.context.bool_type().const_all_ones())
                }

                AstRawType::bool(false) => {
                    BasicValueEnum::IntValue(self.context.bool_type().const_zero())
                }

                AstRawType::str(string) => {
                    BasicValueEnum::PointerValue(self.get_string_ptr(string))
                }
            },
            AstValue::Nil => BasicValueEnum::IntValue(self.context.bool_type().const_int(0, true)), // Represent as a 0 so llvm doesn't panic.  Future optimizations are possible, but this works for now
        }
    }
}
