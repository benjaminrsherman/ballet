use std::fmt;
use std::rc::Rc;

/// Specifies the different types a static value can have.  This also stores the value to
/// simplify the ASTValue type's raw variant.
#[derive(Debug, Clone)]
#[allow(non_camel_case_types)]
pub enum AstRawType {
    float(f64),
    int(i64),
    str(String),
    bool(bool),
}

impl fmt::Display for AstRawType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::float(flt) => write!(f, "{}", flt),
            Self::int(int) => write!(f, "{}", int),
            Self::str(string) => write!(f, "{}", string),
            Self::bool(boolean) => write!(f, "{}", boolean),
        }
    }
}

/// Defines the argument and return types for a function
#[derive(Debug, Clone)]
pub struct AstFunctionType {
    pub arg_types: Vec<AstType>,
    pub ret_type: Rc<AstType>,
}

/// Specifies the different types a value can have
#[derive(Debug, Clone)]
pub enum AstType {
    Nil,
    Raw(AstRawType),
    Function(AstFunctionType),
}

/// Specifies an argument to a function
#[derive(Debug)]
pub struct AstArgument {
    pub name: String,
    pub arg_type: AstType,
}

#[derive(Debug, Clone)]
pub struct AstFunctionPathComponents(pub Vec<String>);

pub fn signature_str(components: &Vec<String>) -> String {
    components
        .iter()
        .skip(1)
        .fold(components[0].clone(), |full_path, component| {
            if !component.is_empty() {
                full_path + "." + component
            } else {
                full_path
            }
        })
}

impl fmt::Display for AstFunctionPathComponents {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", signature_str(&self.0))
    }
}

/// Defines a function prototype
#[derive(Debug)]
pub struct AstSignature {
    pub namespace_components: AstFunctionPathComponents,
    pub args: Vec<AstArgument>,
    pub ret_type: AstType,
}

/// Defines a value type.  Currently, this is either the result of a function call,
/// a variable's value, or a literal value.
#[derive(Debug)]
pub enum AstValue {
    Call {
        fn_type: AstFunctionType,
        fn_call: AstFunctionCall,
    },
    Ident {
        ident_type: AstType,
        binding: String,
    },
    Literal(AstRawType),
    Switch {
        condition: Box<AstValue>,
        true_branch: Box<AstValue>,
        false_branch: Box<AstValue>,
    },
    Nil,
}

impl AstValue {
    pub fn into_type(&self) -> AstType {
        match self {
            AstValue::Call { fn_type, .. } => AstType::Function(fn_type.clone()),
            AstValue::Ident { ident_type, .. } => ident_type.clone(),
            AstValue::Literal(raw_type) => AstType::Raw(raw_type.clone()),
            AstValue::Switch { true_branch, .. } => true_branch.into_type(), // Each branch returns the same type (checked elsewhere)
            AstValue::Nil => AstType::Nil,
        }
    }
}

#[derive(Debug)]
pub enum AstStatement {
    Value(AstValue),
    Bind(String, AstValue),
}

/// Defines a call to a function (essentially a "statement")
#[derive(Debug)]
pub struct AstFunctionCall {
    pub namespace_components: AstFunctionPathComponents,
    pub args: Vec<AstValue>,
}

#[derive(Debug)]
pub struct AstFunction {
    pub signature: AstSignature,
    pub functions: Vec<AstFunction>,
    pub statements: Vec<AstStatement>,
}
