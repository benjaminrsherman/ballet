/// This file ONLY generates an AST from a given token stream and checks for errors.
/// All optimizations occur elsewhere.
use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::ops::{Index, IndexMut};
use std::rc::Rc;

use super::lexer::{LexedToken, Token};
use crate::standard_library::std_signatures;

mod types;
pub use types::*;

/// Maps a function name to the number of arguments it expects
pub type SignatureMap = HashMap<String, AstSignature>;

/// Vector of Tokens
#[derive(Debug, Default)]
pub struct TokenVec(Vec<LexedToken>);

impl TokenVec {
    fn get(&self, pos: usize) -> Result<&LexedToken> {
        if pos >= self.0.len() {
            Err(ParserError {
                line: self.0.last().unwrap().line + 1,
                pos: 0,
                err: "Unexpected EOF".to_string(),
            })
        } else {
            Ok(&self.0[pos])
        }
    }

    fn len(&self) -> usize {
        self.0.len()
    }
}

impl Index<usize> for TokenVec {
    type Output = LexedToken;

    fn index(&self, idx: usize) -> &Self::Output {
        &self.0[idx]
    }
}

impl IndexMut<usize> for TokenVec {
    fn index_mut(&mut self, idx: usize) -> &mut Self::Output {
        &mut self.0[idx]
    }
}

#[derive(Debug, Clone)]
pub struct ParserError {
    err: String,
    line: usize,
    pos: usize,
}

impl ParserError {
    fn from_tok<T: std::string::ToString>(tok: &LexedToken, err: T) -> Self {
        Self {
            line: tok.line,
            pos: tok.pos,
            err: err.to_string(),
        }
    }

    fn unexpected(tok: &LexedToken) -> Self {
        Self {
            line: tok.line,
            pos: tok.pos,
            err: format!("Unexpected token '{}'", tok.tok),
        }
    }
}

impl fmt::Display for ParserError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Parse error on line {} at position {}: {}",
            self.line, self.pos, self.err
        )
    }
}

impl Error for ParserError {}

type Result<T> = std::result::Result<T, ParserError>;

/// Represents the Ballet parser
pub struct Parser {
    signatures: SignatureMap,
    tokens: TokenVec,
    pos: usize,
    current_fn_components: Vec<String>,
    current_binding_types: Vec<HashMap<String, AstType>>,
}

impl Parser {
    pub fn new() -> Result<Self> {
        let mut parser = Parser {
            signatures: SignatureMap::new(),
            tokens: TokenVec::default(),
            pos: 0,
            current_fn_components: vec!["".to_string()],

            // current_binding_types is initialized with a HashMap because bindings in
            // the outermost scope (i.e. not in a function) need a place to be stored
            current_binding_types: vec![HashMap::new()],
        };

        for signature in std_signatures() {
            parser
                .signatures
                .insert(signature.namespace_components.to_string(), signature);
        }

        Ok(parser)
    }

    pub fn parse(
        &mut self,
        tokens: Vec<LexedToken>,
    ) -> Result<(Vec<AstFunction>, Vec<AstStatement>)> {
        self.tokens = TokenVec(tokens);
        self.pos = 0;

        self.parse_signatures()?;
        // TODO: Validate `main`'s signature here
        self.pos = 0;

        // This is needed since the parser thinks we're in a function, so we have to have
        // a terminator at the end of the file.  If there's already an `end` there it's fine
        // since we're not going to try to parse past it anyways.
        self.tokens.0.push(LexedToken {
            tok: Token::End,
            line: self.tokens.0.last().unwrap().line + 1,
            pos: 1,
        });

        self.parse_function_contents()
    }

    fn parse_signatures(&mut self) -> Result<()> {
        while self.pos < self.tokens.len() {
            if let Token::Def = self.tokens.get(self.pos)?.tok {
                let signature = self.parse_signature()?;
                self.signatures
                    .insert(signature.namespace_components.to_string(), signature);
            } else {
                // We can unwrap since we're guaranteed to be in bounds
                if let Token::End = self.tokens.get(self.pos)?.tok {
                    // parse_signature() ends after the signature's `end` token, so if we're
                    // seeing another one here that means the function's scope has ended
                    self.current_fn_components.pop();
                }
                self.pos += 1;
            }
        }

        Ok(())
    }

    /// Parses a `Signature` from a token stream starting with `Token::Def`.
    /// After this function, `pos` will be the index of the first token after `Token::End`.
    fn parse_signature(&mut self) -> Result<AstSignature> {
        self.pos += 1; // move off 'def'

        let function_name = self.tokens.get(self.pos)?;
        let function_name = match &function_name.tok {
            Token::Ident(name) => name,
            Token::FuncCall { .. } => {
                return Err(ParserError::from_tok(
                    function_name,
                    "Function names cannot contain '.'",
                ))
            }
            _ => {
                return Err(ParserError::from_tok(
                    function_name,
                    "Expected function name",
                ))
            }
        };

        self.current_fn_components.push(function_name.clone());

        let mut args = Vec::new();

        loop {
            self.pos += 1;

            let arg_name = self.tokens.get(self.pos)?;
            let arg_name = match &arg_name.tok {
                Token::Ident(name) => name.clone(),
                Token::End => break,
                tok => {
                    return Err(ParserError::from_tok(
                        arg_name,
                        format!("Expected argument name, found '{}'", tok),
                    ))
                }
            };
            self.pos += 1;

            let arg_type = self.parse_type()?;

            args.push(AstArgument {
                name: arg_name,
                arg_type,
            });
        }
        self.pos += 1; // move off end

        let ret_type = self.parse_type()?;

        self.pos += 1; // move off return type

        Ok(AstSignature {
            namespace_components: AstFunctionPathComponents(self.current_fn_components.clone()),
            args,
            ret_type,
        })

        // Because this function doesn't recurse, it's up to the caller to pop this at
        // the right place.  Hopefully there can be a better method of doing this later.
        // self.current_fn_components.pop();
    }

    /// Parses the type at the current location.
    fn parse_type(&self) -> Result<AstType> {
        let token = self.tokens.get(self.pos)?;

        Ok(match &token.tok {
            Token::Ident(arg_type) => AstType::Raw(match arg_type.as_str() {
                "float" => AstRawType::float(0f64),
                "int" => AstRawType::int(0),
                "str" => AstRawType::str(String::new()),
                "bool" => AstRawType::bool(false),

                tok => {
                    return Err(ParserError::from_tok(
                        token,
                        format!("Expected type, found '{}'", tok),
                    ))
                }
            }),
            Token::Nil => AstType::Nil,
            // TODO: Functions as arguments
            _ => {
                return Err(ParserError::from_tok(
                    token,
                    format!("Expected type, found '{}'", token.tok),
                ))
            }
        })
    }

    /// Parses a function starting at `self.pos`.
    fn parse_function(&mut self) -> Result<AstFunction> {
        let signature = self.parse_signature()?;

        // Insert the arguments as typed bindings for the current scope
        let mut map = HashMap::new();
        for arg in signature.args.iter() {
            map.insert(arg.name.clone(), arg.arg_type.clone());
        }
        self.current_binding_types.push(map);

        let (functions, statements) = self.parse_function_contents()?;

        // parse_function_contents() recurses, so we're done with this function
        self.current_binding_types.pop();
        self.current_fn_components.pop();

        Ok(AstFunction {
            signature,
            functions,
            statements,
        })
    }

    fn parse_function_contents(&mut self) -> Result<(Vec<AstFunction>, Vec<AstStatement>)> {
        let mut functions = Vec::new();
        let mut statements = Vec::new();

        // `pos` should be pointing at the first token of the function contents
        // ie the token following the return type

        loop {
            match self.tokens.get(self.pos)?.tok {
                Token::End => break,
                Token::Def => functions.push(self.parse_function()?),
                Token::Bind => statements.push(self.parse_bind()?),
                _ => statements.push(AstStatement::Value(self.parse_value()?)),
            }
        }

        self.pos += 1; // move off 'end'

        Ok((functions, statements))
    }

    fn parse_bind(&mut self) -> Result<AstStatement> {
        self.pos += 1; // move off 'bind'

        let binding = self.tokens.get(self.pos)?;
        let binding = match &binding.tok {
            Token::Ident(ident) => ident.clone(),
            _ => return Err(ParserError::unexpected(binding)),
        };

        self.pos += 1; // move off binding

        let value = self.parse_value()?;

        self.current_binding_types
            .last_mut()
            .unwrap()
            .insert(binding.clone(), value.into_type());

        Ok(AstStatement::Bind(binding, value))
    }

    fn parse_value(&mut self) -> Result<AstValue> {
        let token = self.tokens.get(self.pos)?;

        match &token.tok {
            Token::FuncCall { .. } => self.parse_call(),
            Token::Raw(val) => {
                self.pos += 1;

                Ok(AstValue::Literal(val.clone()))
            }
            Token::Ident(ident) => {
                self.pos += 1;

                Ok(AstValue::Ident {
                    ident_type: self.get_ident_type(token, ident)?,
                    binding: ident.clone(),
                })
            }
            Token::Switch => self.parse_switch(),
            Token::Pass => {
                self.pos += 1;

                Ok(AstValue::Nil)
            }
            _ => Err(ParserError::unexpected(token)),
        }
    }

    fn parse_call(&mut self) -> Result<AstValue> {
        let fn_name_tok = self.tokens.get(self.pos)?;
        let fn_name = match &fn_name_tok.tok {
            Token::FuncCall {
                namespace_components,
            } => {
                if namespace_components[0].is_empty() {
                    // This is a global call
                    signature_str(namespace_components)
                } else {
                    return Err(ParserError::from_tok(
                        fn_name_tok,
                        "Non-global function calls are currently unsupported",
                    ));
                }
            }
            _ => {
                return Err(ParserError::from_tok(
                    fn_name_tok,
                    format!("Expected function name, found {}", fn_name_tok.tok),
                ))
            }
        };

        let (namespace_components, arg_types, ret_type) = match self.signatures.get(&fn_name) {
            Some(sig) => (
                sig.namespace_components.clone(),
                sig.args
                    .iter()
                    .map(|arg| arg.arg_type.clone())
                    .collect::<Vec<AstType>>(),
                sig.ret_type.clone(),
            ),
            None => {
                return Err(ParserError::from_tok(
                    fn_name_tok,
                    format!("Could not find function signature for '{}'", fn_name),
                ))
            }
        };

        let num_args = arg_types.len();

        let mut args = Vec::with_capacity(num_args);

        self.pos += 1; // Move off function name

        for _ in 0..num_args {
            let token = self.tokens.get(self.pos)?;
            match &token.tok {
                Token::Raw(val) => {
                    self.pos += 1;

                    args.push(AstValue::Literal(val.clone()));
                }
                Token::Ident(ident) => {
                    self.pos += 1;

                    args.push(AstValue::Ident {
                        ident_type: self.get_ident_type(token, ident)?,
                        binding: ident.clone(),
                    });
                }
                Token::FuncCall { .. } => args.push(self.parse_call()?), // don't increment since it happens internally
                _ => return Err(ParserError::unexpected(token)),
            };
        }

        Ok(AstValue::Call {
            fn_type: AstFunctionType {
                arg_types,
                ret_type: Rc::new(ret_type),
            },

            fn_call: AstFunctionCall {
                namespace_components,
                args,
            },
        })
    }

    fn parse_switch(&mut self) -> Result<AstValue> {
        self.pos += 1; // move off 'switch'
        let condition = self.parse_value()?;
        let true_branch = self.parse_value()?;
        let false_branch = self.parse_value()?;

        /* TODO: implement Eq for AstType
        if true_branch.into_type() != false_branch.into_type() {
            return Err(format!(
                "True branch {:?} doesn't match false branch {:?}",
                true_branch, false_branch
            ));
        }
        */

        Ok(AstValue::Switch {
            condition: Box::new(condition),
            true_branch: Box::new(true_branch),
            false_branch: Box::new(false_branch),
        })
    }

    fn get_ident_type(&self, tok: &LexedToken, ident: &str) -> Result<AstType> {
        for bindings in &self.current_binding_types {
            if let Some(t) = bindings.get(ident) {
                return Ok(t.clone());
            }
        }

        Err(ParserError::from_tok(
            tok,
            format!(
                "Identifier '{}' hasn't been defined before this point",
                ident
            ),
        ))
    }
}
