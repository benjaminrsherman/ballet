mod lexer;
use lexer::*;

mod parser;
pub use parser::*;

pub fn parse_str(
    input: &str,
    parser: Option<&mut Parser>,
) -> Result<(Vec<AstFunction>, Vec<AstStatement>), ParserError> {
    let tokens: Vec<LexedToken> = Lexer::new(input).collect();
    match parser {
        Some(parser) => parser.parse(tokens),
        None => Parser::new()?.parse(tokens),
    }
}
