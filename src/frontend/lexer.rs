/// Lexes an input char stream into parseable tokens
use std::error::Error;
use std::fmt;
use std::iter::Peekable;
use std::ops::DerefMut;
use std::str::Chars;

use itertools::Itertools;

use super::parser::AstRawType;

/// Represents a primitive syntax token
#[derive(Debug, Clone)]
pub enum Token {
    Def,
    End,
    Raw(AstRawType),
    Ident(String),
    FuncCall { namespace_components: Vec<String> },
    Bind,
    Switch,
    Pass,
    Nil,
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Def => write!(f, "def"),
            Self::End => write!(f, "end"),
            Self::Raw(val) => write!(f, "{}", val),
            Self::Ident(ident) => write!(f, "{}", ident),
            Self::FuncCall {
                namespace_components,
            } => write!(f, "{}", namespace_components.iter().format(".")),
            Self::Bind => write!(f, "bind"),
            Self::Switch => write!(f, "switch"),
            Self::Pass => write!(f, "pass"),
            Self::Nil => write!(f, "nil"),
        }
    }
}

#[derive(Debug, Clone)]
pub struct LexedToken {
    pub tok: Token,
    pub line: usize,
    pub pos: usize,
}

#[derive(Debug)]
struct LexError {
    err: String,
    line: usize,
    pos: usize,
}

impl fmt::Display for LexError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Lex error on line {} at position {}: {}",
            self.line, self.pos, self.err
        )
    }
}

impl Error for LexError {}

type LexResult = Result<Option<LexedToken>, LexError>;

/// Defines a lexer which transforms an input `String` into
/// a `Token` stream.
pub struct Lexer<'inpt> {
    input: &'inpt str,
    chars: Box<Peekable<Chars<'inpt>>>,
    pos: usize,

    line: usize,
    line_pos: usize,
}

impl<'inpt> Lexer<'inpt> {
    /// Creates a new `Lexer`, given its source `input`.
    pub fn new(input: &'inpt str) -> Lexer<'inpt> {
        Lexer {
            input,
            chars: Box::new(input.chars().peekable()),
            pos: 0,
            line: 1,
            line_pos: 0,
        }
    }

    /// Lexes and returns the next `Token` from the source code.
    fn lex(&mut self) -> LexResult {
        let chars = self.chars.deref_mut();

        // Skip whitespaces
        loop {
            // Note: the following lines are in their own scope to
            // limit how long 'chars' is borrowed, and in order to allow
            // it to be borrowed again in the loop by 'chars.next()'.
            {
                let ch = chars.peek();

                if ch.is_none() {
                    return Ok(None);
                }

                if !ch.unwrap().is_whitespace() && !is_newline(ch.unwrap()) {
                    break;
                }
            }

            if is_newline(&chars.next().unwrap()) {
                self.line += 1;
                self.line_pos = 0;
            }

            self.pos += 1;
            self.line_pos += 1;
        }

        let start = self.pos;
        let next = chars.next();

        if next.is_none() {
            return Ok(None);
        }

        self.pos += 1;
        self.line_pos += 1;

        let tok_line_start = self.line;
        let tok_line_pos_start = self.line_pos - 1; // We're shifted forward by 1 above

        let next_tok = match next.unwrap() {
            '0'..='9' | '-' => {
                if let Some('-') = chars.peek() {
                    // This is a comment
                    while !is_newline(chars.peek().unwrap_or(&'\n')) {
                        self.pos += 1;
                        chars.next();
                    }
                    self.line += 1;
                    self.line_pos = 0;

                    // Skip over this comment and lex the next token
                    return self.lex();
                }

                match lex_numeric(start, &mut self.pos, chars, self.input) {
                    Ok(val) => Ok(Token::Raw(val)),
                    Err(e) => Err(format!("Error parsing literal ({})", e.0)),
                }
            }

            '\'' => {
                loop {
                    let next_char = chars.next().unwrap_or('\'');

                    self.pos += 1;
                    self.line_pos += 1;

                    if is_newline(&next_char) {
                        self.line += 1;
                        self.line_pos = 0;
                    } else if next_char == '\'' {
                        break;
                    }
                }

                Ok(Token::Raw(AstRawType::str(
                    self.input[start + 1..self.pos - 1].to_string(),
                )))
            }

            _ => {
                while !chars.peek().unwrap_or(&' ').is_whitespace() {
                    chars.next();

                    self.pos += 1;
                    self.line_pos += 1;
                }

                match &self.input[start..self.pos] {
                    "def" => Ok(Token::Def),
                    "end" => Ok(Token::End),
                    "bind" => Ok(Token::Bind),
                    "switch" => Ok(Token::Switch),
                    "pass" => Ok(Token::Pass),
                    "true" => Ok(Token::Raw(AstRawType::bool(true))),
                    "false" => Ok(Token::Raw(AstRawType::bool(false))),
                    "nil" => Ok(Token::Nil),
                    tok => {
                        let tok_str = tok.to_string();

                        if tok_str.contains('.') {
                            let namespace_components = self.input[start..self.pos]
                                .split(".")
                                .map(String::from)
                                .collect();

                            Ok(Token::FuncCall {
                                namespace_components,
                            })
                        } else {
                            Ok(Token::Ident(tok_str))
                        }
                    }
                }
            }
        };

        match next_tok {
            Ok(tok) => Ok(Some(LexedToken {
                line: tok_line_start,
                pos: tok_line_pos_start,
                tok,
            })),
            Err(err) => Err(LexError {
                line: tok_line_start,
                pos: tok_line_pos_start,
                err,
            }),
        }
    }
}

impl<'a> Iterator for Lexer<'a> {
    type Item = LexedToken;

    /// Lexes the next `Token` and returns it.
    /// On EOF or failure, `None` will be returned.
    fn next(&mut self) -> Option<Self::Item> {
        match self.lex() {
            Ok(token) => token,
            Err(e) => {
                eprintln!("{}", e);
                None
            }
        }
    }
}

fn is_newline(c: &char) -> bool {
    match c {
        '\n' | '\r' => true,
        _ => false,
    }
}

struct LexNumericErr(String);

impl From<std::num::ParseIntError> for LexNumericErr {
    fn from(e: std::num::ParseIntError) -> Self {
        LexNumericErr(format!("{}", e))
    }
}

impl From<std::num::ParseFloatError> for LexNumericErr {
    fn from(e: std::num::ParseFloatError) -> Self {
        LexNumericErr(format!("{}", e))
    }
}

fn lex_numeric(
    start: usize,
    end_pos: &mut usize,
    chars: &mut Peekable<Chars>,
    input: &str,
) -> Result<AstRawType, LexNumericErr> {
    loop {
        let curr_char = *chars.peek().unwrap_or(&' ');

        if !curr_char.is_numeric()
            && curr_char != '.'
            && curr_char != 'u'
            && curr_char != 'i'
            && curr_char != 'f'
        {
            break;
        }

        chars.next();
        *end_pos += 1;
    }

    let num_str = input[start..*end_pos].to_string();

    Ok(if num_str.contains(".") {
        AstRawType::float(num_str[..num_str.len()].parse()?)
    } else {
        AstRawType::int(num_str[..num_str.len()].parse()?)
    })
}
