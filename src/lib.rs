#[macro_use]
extern crate anyhow;

mod backend;
mod frontend;
pub mod standard_library;

pub mod tests;

use backend::Compiler;
use frontend::Parser;

use std::fs;
use std::io::Write;
use std::path::PathBuf;

use anyhow::Result;
use structopt::StructOpt;

use inkwell::context::Context;
use inkwell::OptimizationLevel;

#[derive(StructOpt)]
pub struct PerformOpts {
    #[structopt(parse(from_os_str))]
    input_file: PathBuf,

    #[structopt(long = "print_llvm")]
    print_llvm_bitcode: bool,
}

#[derive(StructOpt)]
pub struct ReplOpts {
    #[structopt(long = "print_llvm")]
    print_llvm_bitcode: bool,
}

pub fn execute_str(script: &str, mut compiler: Compiler) -> Result<()> {
    let (functions, statements) = frontend::parse_str(script, None)?;

    compiler.compile(functions, statements, true);

    let ee = compiler
        .module
        .create_jit_execution_engine(OptimizationLevel::Aggressive)
        .unwrap();
    let main = unsafe { ee.get_function::<unsafe extern "C" fn() -> i8>(".__main") }.unwrap();

    unsafe {
        main.call();
    }

    Ok(())
}

pub fn execute_script(opts: PerformOpts) -> Result<()> {
    let file_contents = match fs::read_to_string(&opts.input_file) {
        Ok(fc) => fc,
        Err(e) => {
            return Err(anyhow!(
                "Error reading script from '{}': {}",
                opts.input_file.as_path().display(),
                e
            ));
        }
    };

    execute_str(
        file_contents.as_str(),
        Compiler::new(&Context::create(), opts.print_llvm_bitcode),
    )
}

pub fn repl(opts: ReplOpts) -> Result<()> {
    let stdin = std::io::stdin();
    let context = Context::create();
    let mut compiler = Compiler::new(&context, opts.print_llvm_bitcode);
    let mut repl_fn_count = 0;

    let mut parser = Parser::new()?;

    loop {
        print!("> ");
        std::io::stdout().lock().flush().unwrap();

        let mut line = String::new();
        if stdin.read_line(&mut line).unwrap() == 0 {
            break; // break on EOF
        }

        if line.chars().filter(|c| !c.is_whitespace()).count() == 0 {
            continue;
        }

        let (functions, statements) = match frontend::parse_str(&line, Some(&mut parser)) {
            Ok(ret) => ret,
            Err(e) => {
                println!("{}", e);
                continue;
            }
        };

        compiler.compile(functions, Vec::new(), false);

        use crate::frontend::{
            AstFunction, AstFunctionPathComponents, AstSignature, AstStatement, AstType,
        };
        for statement in statements {
            match statement {
                AstStatement::Bind(binding, val) => {
                    let bound_val = compiler.compile_value(&val);
                    compiler.bindings.insert(binding, bound_val);
                }
                AstStatement::Value(ref val) => {
                    let func_name = format!("__repl_function_{}", repl_fn_count);
                    let ret_type = match val.into_type() {
                        AstType::Function(func) => (*func.ret_type).clone(),
                        ret_type => ret_type,
                    };

                    // Create and execute anonymous function
                    let function = AstFunction {
                        signature: AstSignature {
                            namespace_components: AstFunctionPathComponents(
                                vec![func_name.clone()],
                            ),
                            args: Vec::new(),
                            ret_type,
                        },
                        functions: Vec::new(),
                        statements: vec![statement],
                    };

                    compiler.compile(vec![function], Vec::new(), false);

                    let ee = compiler
                        .module
                        .create_jit_execution_engine(OptimizationLevel::Aggressive)
                        .unwrap();
                    let func =
                        unsafe { ee.get_function::<unsafe extern "C" fn() -> i8>(&func_name) }
                            .unwrap();

                    unsafe {
                        func.call();
                    }

                    repl_fn_count += 1;
                }
            }
        }
    }

    Ok(())
}
