macro_rules! ast_type {
    (nil $llvm_type:ident $($auxiliary_llvm_types:ident)*) => {
        AstType::Nil
    };

    (str $llvm_type:ident $($auxiliary_llvm_types:ident)*) => {
        AstType::Raw(AstRawType::str("".to_string()))
    };

    ($blt_type:ident $llvm_type:ident $($auxiliary_llvm_types:ident)*) => {
        AstType::Raw(AstRawType::$blt_type($llvm_type::default()))
    };
}

macro_rules! stl {
    (
        $(
            $(#[$attr:meta])*
            fn ($($component:ident)*) $name:ident($c:ident, $args:ident, $func:ident)
                (
                    $(
                        $blt_arg_type:ident

                        $((
                            $llvm_arg_type:ident $($llvm_ctor_arg:expr)*
                         ))+

                    ),*
                )
                -> $blt_ret_type:ident $llvm_ret_type:ident $($llvm_ret_arg:ident)?
            $contents:block
        )*
    )
        => {

use $crate::frontend::*;

pub fn std_signatures() -> Vec<AstSignature> {
    let mut ret = Vec::new();
    $({
        let namespace_components = AstFunctionPathComponents(vec![
            "".to_string(),
            $(stringify!($component).to_string()),*
        ]);

        let args = vec![$(
            AstArgument {
                name: "".to_string(), // We manually create the bitcode, so no arg names are needed
                arg_type: ast_type!($blt_arg_type $($llvm_arg_type)*),
            }
        ,)*];

        ret.push(AstSignature {
            namespace_components,
            args,
            ret_type: ast_type!($blt_ret_type $llvm_ret_type),
        });
    })*
    ret
}

pub fn compile_std(c: &mut crate::backend::Compiler) {
    let int_type = c.context.i32_type();

    let const_char_ptr_type = c.context.i8_type().ptr_type(AddressSpace::Generic);
    let printf_type =
        int_type.fn_type(&[BasicTypeEnum::PointerType(const_char_ptr_type)], true);

    c.register_extern("printf", printf_type);

    $(
        $name(c).unwrap();
    )*
}

$(
    $(#[$attr])*
    pub fn $name($c: &crate::backend::Compiler) -> Result<(), String> {
        let fn_name = concat!($(".", stringify!($component)),* );

        let ret_type = paste::expr!($c.context.[<$llvm_ret_type _type>]($($llvm_ret_arg)*));
        let fn_type = ret_type.fn_type(&[
                $(
                    paste::expr!(
                        $c.context
                        $(
                            .[<$llvm_arg_type _type>]($($llvm_ctor_arg),*)
                        )*
                        .into()
                    ),
                )*
            ], false);

        let $func =
            $c.module.add_function(fn_name, fn_type, None);

        let basic_block = $c.context.append_basic_block($func, "entry");

        $c.builder.position_at_end(basic_block);

        let $args = $func.get_params();
        let return_val = $contents;

        $c.builder.build_return(Some(&return_val));

        if !$func.verify(true) {
            return Err(format!("Unable to generate standard function '{}'", stringify!($name)))
        } else {
            Ok(())
        }
    }
)*

    };
}
