use crate::backend::Compiler;
use inkwell::types::BasicTypeEnum;
use inkwell::values::{BasicValueEnum, FunctionValue};
use inkwell::AddressSpace;
use inkwell::{FloatPredicate, IntPredicate};

#[macro_use]
mod macros;

stl! {
    /// Returns `true` if two integers are equal and `false` otherwise
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("true\n", "
    /// # .bool.print
    /// .int.eq 3 3 -- returns true
    /// # ");
    /// # ballet_validate!("false\n", "
    /// # .bool.print
    /// .int.eq 3 4 -- returns false
    /// # ");
    /// # Ok::<(), String>(())
    /// ```
    fn (int eq) int_eq(c, args, func)(int (i64), int (i64)) -> bool bool {
        let arg0 = args[0].into_int_value();
        let arg1 = args[1].into_int_value();
        let cmp = c.builder.build_int_compare(IntPredicate::EQ, arg0, arg1, "cmp");

        let int_type = c.context.bool_type();

        c.builder.build_int_cast(cmp, int_type, "bool_cast")
    }

    /// Returns `true` if two floats are equal and `false` otherwise
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("true\n", "
    /// # .bool.print
    /// .float.eq 3.0 3.0 -- returns true
    /// # ");
    /// # ballet_validate!("false\n", "
    /// # .bool.print
    /// .float.eq 3.0 4.0 -- returns false
    /// # ");
    /// # Ok::<(), String>(())
    /// ```
    fn (float eq) float_eq(c, args, func)(float (f64), float (f64)) -> bool bool {
        let arg0 = args[0].into_float_value();
        let arg1 = args[1].into_float_value();
        let cmp = c.builder.build_float_compare(FloatPredicate::OEQ, arg0, arg1, "cmp");

        let int_type = c.context.bool_type();

        c.builder.build_int_cast(cmp, int_type, "bool_cast")
    }


    /// Returns `true` if two booleans are equal and `false` otherwise
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("true\n", "
    /// # .bool.print
    /// .bool.eq false false -- returns true
    /// # ");
    /// # ballet_validate!("false\n", "
    /// # .bool.print
    /// .bool.eq true false -- returns false
    /// # ");
    /// # Ok::<(), String>(())
    /// ```
    fn (bool eq) bool_eq(c, args, func)(bool (bool), bool (bool)) -> bool bool {
        let arg0 = args[0].into_int_value();
        let arg1 = args[1].into_int_value();
        let cmp = c.builder.build_int_compare(IntPredicate::EQ, arg0, arg1, "cmp");

        let int_type = c.context.bool_type();

        c.builder.build_int_cast(cmp, int_type, "bool_cast")
    }

    /// Returns the sum of two integers
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("5\n", "
    /// # .int.print
    /// .int.add 1 4 -- returns 5
    /// # ");
    ///
    /// # ballet_validate!("-2\n", "
    /// # .int.print
    /// .int.add -3 1 -- returns -2
    /// # ");
    /// # Ok::<(), String>(())
    /// ```
    fn (int add) int_add(c, args, func)(int (i64), int (i64)) -> int i64 {
        let arg0 = args[0].into_int_value();
        let arg1 = args[1].into_int_value();
        c.builder.build_int_add(arg0, arg1, "summed")
    }

    /// Returns the sum of two floats
    /// ```text
    /// .float.add 1.27 3.4 -- returns 4.67
    /// ```
    fn (float add) float_add(c, args, func)(float (f64), float (f64)) -> float f64 {
        let arg0 = args[0].into_float_value();
        let arg1 = args[1].into_float_value();
        c.builder.build_float_add(arg0, arg1, "summed")
    }

    /// Returns the difference of two integers
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("1\n", "
    /// # .int.print
    /// .int.sub 5 4 -- returns 1
    /// # ");
    ///
    /// # ballet_validate!("5\n", "
    /// # .int.print
    /// .int.sub -2 -7 -- returns 5
    /// # ");
    /// # Ok::<(), String>(())
    /// ```
    fn (int sub) int_sub(c, args, func)(int (i64), int (i64)) -> int i64 {
        let arg0 = args[0].into_int_value();
        let arg1 = args[1].into_int_value();
        c.builder.build_int_sub(arg0, arg1, "diffed")
    }

    /// Returns the difference of two floats
    /// ```text
    /// # no tests here due to floating point approximations
    /// .float.sub 1.27 3.4 -- returns -2.13
    /// ```
    fn (float sub) float_sub(c, args, func)(float (f64), float (f64)) -> float f64 {
        let arg0 = args[0].into_float_value();
        let arg1 = args[1].into_float_value();
        c.builder.build_float_sub(arg0, arg1, "diffed")
    }

    /// Returns the product of two integers
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("12\n", "
    /// # .int.print
    /// .int.mul 3 4 -- returns 12
    /// # ");
    ///
    /// # ballet_validate!("-15\n", "
    /// # .int.print
    /// .int.mul -5 3 -- returns -15
    /// # ");
    /// # Ok::<(), String>(())
    /// ```
    fn (int mul) int_mul(c, args, func)(int (i64), int (i64)) -> int i64 {
        let arg0 = args[0].into_int_value();
        let arg1 = args[1].into_int_value();
        c.builder.build_int_mul(arg0, arg1, "multed")
    }

    /// Returns the product of two floats
    /// ```text
    /// # no tests here due to floating point approximations
    /// .float.mul 15.65 8.7 -- returns 136.155
    /// ```
    fn (float mul) float_mul(c, args, func)(float (f64), float (f64)) -> float f64 {
        let arg0 = args[0].into_float_value();
        let arg1 = args[1].into_float_value();
        c.builder.build_float_mul(arg0, arg1, "multed")
    }

    /// Returns the floored division of two integers
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("5\n", "
    /// # .int.print
    /// .int.div 16 3 -- returns 5
    /// # ");
    /// ```
    fn (int div) int_div(c, args, func)(int (i64), int (i64)) -> int i64 {
        let arg0 = args[0].into_int_value();
        let arg1 = args[1].into_int_value();
        c.builder.build_int_signed_div(arg0, arg1, "dived")
    }

    /// Returns the division of two floats
    /// ```text
    /// # no tests here due to floating point approximations
    /// .float.div 8.5 2.0 -- returns 4.25
    /// ```
    fn (float div) float_div(c, args, func)(float (f64), float (f64)) -> float f64 {
        let arg0 = args[0].into_float_value();
        let arg1 = args[1].into_float_value();
        c.builder.build_float_div(arg0, arg1, "dived")
    }

    /// Returns the modulus of two integers
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("1\n", "
    /// # .int.print
    /// .int.mod 16 3 -- returns 1
    /// # ");
    /// ```
    fn (int mod) int_mod(c, args, func)(int (i64), int (i64)) -> int i64 {
        let arg0 = args[0].into_int_value();
        let arg1 = args[1].into_int_value();
        c.builder.build_int_signed_rem(arg0, arg1, "modded")
    }

    /// Returns the modulus of two floats
    /// ```text
    /// # no tests here due to floating point approximations
    /// .float.div 8.5 2.0 -- returns 0.5
    /// ```
    fn (float mod) float_mod(c, args, func)(float (f64), float (f64)) -> float f64 {
        let arg0 = args[0].into_float_value();
        let arg1 = args[1].into_float_value();
        c.builder.build_float_rem(arg0, arg1, "modded")
    }

    /// Returns the bitwise and of two integers
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("1\n", "
    /// # .int.print
    /// .int.and 3 5 -- returns 1
    /// # ");
    /// ```
    fn (int and) int_and(c, args, func)(int (i64), int (i64)) -> int i64 {
        let arg0 = args[0].into_int_value();
        let arg1 = args[1].into_int_value();
        c.builder.build_and(arg0, arg1, "anded")
    }

    /// Returns the and of two booleans
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("true\n", "
    /// # .bool.print
    /// .bool.and true true -- returns true
    /// # ");
    /// # ballet_validate!("false\n", "
    /// # .bool.print
    /// .bool.and false true -- returns false
    /// # ");
    /// ```
    fn (bool and) bool_and(c, args, func)(bool (bool), bool (bool)) -> bool bool {
        let arg0 = args[0].into_int_value();
        let arg1 = args[1].into_int_value();
        let anded = c.builder.build_and(arg0, arg1, "anded");

        let bool_type = c.context.bool_type();

        c.builder.build_int_cast(anded, bool_type, "bool_cast")
    }

    /// Returns the bitwise or of two integers
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("7\n", "
    /// # .int.print
    /// .int.or 3 5 -- returns 7
    /// # ");
    /// ```
    fn (int or) int_or(c, args, func)(int (i64), int (i64)) -> int i64 {
        let arg0 = args[0].into_int_value();
        let arg1 = args[1].into_int_value();
        c.builder.build_or(arg0, arg1, "ored")
    }

    /// Returns the or of two booleans
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("true\n", "
    /// # .bool.print
    /// .bool.or true false -- returns true
    /// # ");
    /// # ballet_validate!("false\n", "
    /// # .bool.print
    /// .bool.or false false -- returns false
    /// # ");
    /// ```
    fn (bool or) bool_or(c, args, func)(bool (bool), bool (bool)) -> bool bool {
        let arg0 = args[0].into_int_value();
        let arg1 = args[1].into_int_value();
        let ored = c.builder.build_or(arg0, arg1, "ored");

        let bool_type = c.context.bool_type();

        c.builder.build_int_cast(ored, bool_type, "bool_cast")
    }

    /// Returns the bitwise xor of two integers
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("6\n", "
    /// # .int.print
    /// .int.xor 3 5 -- returns 6
    /// # ");
    /// ```
    fn (int xor) int_xor(c, args, func)(int (i64), int (i64)) -> int i64 {
        let arg0 = args[0].into_int_value();
        let arg1 = args[1].into_int_value();
        c.builder.build_xor(arg0, arg1, "xored")
    }

    /// Returns the xor of two booleans
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("true\n", "
    /// # .bool.print
    /// .bool.xor true false -- returns true
    /// # ");
    /// # ballet_validate!("false\n", "
    /// # .bool.print
    /// .bool.xor true true -- returns false
    /// # ");
    /// ```
    fn (bool xor) bool_xor(c, args, func)(bool (bool), bool (bool)) -> bool bool {
        let arg0 = args[0].into_int_value();
        let arg1 = args[1].into_int_value();
        let xored = c.builder.build_xor(arg0, arg1, "xored");

        let bool_type = c.context.bool_type();

        c.builder.build_int_cast(xored, bool_type, "bool_cast")
    }

    /// Returns the first integer shifted left by the second integer
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("96\n", "
    /// # .int.print
    /// .int.lshift 3 5 -- returns 96
    /// # ");
    /// ```
    fn (int lshift) int_lshift(c, args, func)(int (i64), int (i64)) -> int i64 {
        let arg0 = args[0].into_int_value();
        let arg1 = args[1].into_int_value();
        c.builder.build_left_shift(arg0, arg1, "lshifted")
    }

    /// Returns the first integer shifted right by the second integer
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("3\n", "
    /// # .int.print
    /// .int.rshift 96 5 -- returns 3
    /// # ");
    /// ```
    fn (int rshift) int_rshift(c, args, func)(int (i64), int (i64)) -> int i64 {
        let arg0 = args[0].into_int_value();
        let arg1 = args[1].into_int_value();
        c.builder.build_right_shift(arg0, arg1, true, "rshifted")
    }

    /// Prints a single integer
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("5\n", "
    /// .int.print 5
    /// # ");
    /// ```
    fn (int print) int_print(c, args, func)(int (i64)) -> nil bool {
        let (printf, printf_str) = register_printf_i64(c);

        c.builder.build_call(printf, &[printf_str, args[0]], "printf_call");

        c.context.bool_type().const_int(0, false)
    }

    /// Prints a single string
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("hello, world\n", "
    /// .str.print 'hello, world'
    /// # ");
    /// ```
    fn (str print) str_print(c, args, func)(str (i8) (ptr AddressSpace::Generic)) -> nil bool {
        let (printf, printf_str) = register_printf_str(c);

        c.builder.build_call(printf, &[printf_str, args[0]], "printf_call");

        c.context.bool_type().const_int(0, false)
    }

    /// Prints a single float
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("0.100000\n", "
    /// .float.print 0.1
    /// # ");
    /// ```
    fn (float print) float_print(c, args, func)(float (f64)) -> nil bool {
        let (printf, printf_str) = register_printf_flt(c);

        c.builder.build_call(printf, &[printf_str, args[0]], "printf_call");

        c.context.bool_type().const_int(0, false)
    }

    /// Prints a single boolean
    /// ```
    /// # use ballet::ballet_validate;
    /// # ballet_validate!("true\n", "
    /// .bool.print true
    /// # ");
    /// # ballet_validate!("false\n", "
    /// .bool.print false
    /// # ");
    /// ```
    fn (bool print) bool_print(c, args, func)(bool (bool)) -> nil bool {
        let (printf, true_printf_str, false_printf_str) = register_printf_bool(c);

        let bb_ret = c.context.append_basic_block(func, "ret");
        let bb_true = c.context.prepend_basic_block(bb_ret, "print_true");
        let bb_false = c.context.prepend_basic_block(bb_ret, "print_false");

        let false_val = c.context.bool_type().const_zero();
        let cmp = c.builder.build_int_compare(IntPredicate::EQ, false_val, args[0].into_int_value(), "bool_cmp");
        c.builder.build_conditional_branch(cmp, bb_false, bb_true);

        c.builder.position_at_end(bb_true);
        c.builder.build_call(printf, &[true_printf_str], "print_true");
        c.builder.build_unconditional_branch(bb_ret);

        c.builder.position_at_end(bb_false);
        c.builder.build_call(printf, &[false_printf_str], "print_false");
        c.builder.build_unconditional_branch(bb_ret);

        c.builder.position_at_end(bb_ret);
        c.context.bool_type().const_int(0, false)
    }
}

// ######################################################################################
// STL HELPER FUNCTIONS #################################################################
// ######################################################################################

fn register_printf_i64<'ctx>(c: &'ctx Compiler) -> (FunctionValue<'ctx>, BasicValueEnum<'ctx>) {
    let printf = *c.get_extern("printf").unwrap();
    let printf_str = c.builder.build_global_string_ptr("%d\n", "printf_str");

    (
        printf,
        BasicValueEnum::PointerValue(printf_str.as_pointer_value()),
    )
}

fn register_printf_str<'ctx>(c: &'ctx Compiler) -> (FunctionValue<'ctx>, BasicValueEnum<'ctx>) {
    let printf = *c.get_extern("printf").unwrap();
    let printf_str = c.builder.build_global_string_ptr("%s\n", "printf_str");

    (
        printf,
        BasicValueEnum::PointerValue(printf_str.as_pointer_value()),
    )
}

fn register_printf_flt<'ctx>(c: &'ctx Compiler) -> (FunctionValue<'ctx>, BasicValueEnum<'ctx>) {
    let printf = *c.get_extern("printf").unwrap();
    let printf_str = c.builder.build_global_string_ptr("%lf\n", "printf_str");

    (
        printf,
        BasicValueEnum::PointerValue(printf_str.as_pointer_value()),
    )
}

fn register_printf_bool<'ctx>(
    c: &'ctx Compiler,
) -> (
    FunctionValue<'ctx>,
    BasicValueEnum<'ctx>,
    BasicValueEnum<'ctx>,
) {
    let printf = *c.get_extern("printf").unwrap();
    let true_printf_str = c.builder.build_global_string_ptr("true\n", "printf_str");
    let false_printf_str = c.builder.build_global_string_ptr("false\n", "printf_str");

    (
        printf,
        BasicValueEnum::PointerValue(true_printf_str.as_pointer_value()),
        BasicValueEnum::PointerValue(false_printf_str.as_pointer_value()),
    )
}
