#[macro_export]
macro_rules! ballet_validate {
    ($expected:literal, $blt_source:literal) => {
        ballet::tests::run_test($blt_source, $expected, "");
    };
}
