#[macro_use]
mod macros;
pub use macros::*;

use nix::unistd::*;
use std::ffi::CString;
use std::io::Write;
use std::os::unix::io::RawFd;

use inkwell::context::Context;

use crate::backend::Compiler;

fn read_to_string(fd: RawFd) -> nix::Result<String> {
    let mut vec = Vec::new();
    loop {
        let mut arr = [0u8; 32];
        let num_read = read(fd, &mut arr)?;
        vec.extend_from_slice(&arr[..num_read]);

        if num_read < 32 {
            break;
        }
    }

    Ok(unsafe { CString::from_vec_unchecked(vec).into_string().unwrap() })
}

pub fn run_test(script: &str, expected_output_stdout: &str, expected_output_stderr: &str) {
    let (stdout_read_end, stdout_write_end) = pipe().unwrap();
    let (stderr_read_end, stderr_write_end) = pipe().unwrap();

    if let Ok(ForkResult::Child) = fork() {
        close(stdout_read_end).unwrap();
        close(stderr_read_end).unwrap();

        dup2(stdout_write_end, 1).unwrap();
        dup2(stderr_write_end, 2).unwrap();

        if let Err(e) = crate::execute_str(script, Compiler::new(&Context::create(), false)) {
            write(
                stderr_write_end,
                format!("Error executing script: {}", e).as_str().as_bytes(),
            )
            .unwrap();
        };

        std::io::stdout().lock().flush().unwrap();
        std::io::stderr().lock().flush().unwrap();

        std::process::exit(0);
    }

    close(stdout_write_end).unwrap();
    close(stderr_write_end).unwrap();

    let stderr_res = read_to_string(stderr_read_end).unwrap();
    println!("Compiler error output: {:?}", stderr_res);
    close(stderr_read_end).unwrap();

    let stdout_res = read_to_string(stdout_read_end).unwrap();
    println!("Compiler standard output: {:?}\n", stdout_res);
    close(stdout_read_end).unwrap();

    assert_eq!(stdout_res, expected_output_stdout);
    assert_eq!(stderr_res, expected_output_stderr);
}

#[test]
fn print_str() {
    run_test(".str.print 'hello, world'", "hello, world\n", "");
}

#[test]
fn print_str_multiline() {
    run_test(
        ".str.print 'line 1
line 2'",
        "line 1\nline 2\n",
        "",
    );
}

#[test]
fn print_str_empty() {
    run_test(".str.print ''", "\n", "");
}

#[test]
fn print_int() {
    run_test(".int.print 0", "0\n", "");
}

#[test]
fn print_int_neg() {
    run_test(".int.print -1", "-1\n", "");
}

#[test]
fn print_int_nonzero() {
    run_test(".int.print 1", "1\n", "");
}

#[test]
fn print_int_large() {
    run_test(".int.print 761385", "761385\n", "");
}

#[test]
fn print_float() {
    run_test(".float.print 0.1", "0.100000\n", "");
}

#[test]
fn print_bool_true() {
    run_test(".bool.print true", "true\n", "");
}

#[test]
fn add_int_1_2() {
    run_test(".int.print .int.add 1 2", "3\n", "");
}

#[test]
fn add_int_neg1_2() {
    run_test(".int.print .int.add -1 2", "1\n", "");
}

#[test]
fn add_int_neg1_neg3() {
    run_test(".int.print .int.add -1 -3", "-4\n", "");
}

#[test]
fn add_int_1_neg3() {
    run_test(".int.print .int.add 1 -3", "-2\n", "");
}
