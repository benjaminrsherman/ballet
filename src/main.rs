use ballet::{PerformOpts, ReplOpts};
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(rename_all = "kebab-case")]
enum Ballet {
    /// Execute a Ballet script
    Perform(PerformOpts),

    /// Create a REPL
    Repl(ReplOpts),
}

fn main() {
    if let Err(e) = match Ballet::from_args() {
        Ballet::Perform(opt) => ballet::execute_script(opt),
        Ballet::Repl(opt) => ballet::repl(opt),
    } {
        eprintln!("\nError: {}", e);
    }
}
